{ config, pkgs, pkgs-unstable, ...}:

{

  imports = [
    ../../modules/home/hyprland
    ../../modules/home/waybar
    ../../modules/home/kitty
    ../../modules/home/firefox.nix
    ../../modules/home/editor/neovim
    ../../modules/home/alacritty
  ];

  home.username = "avengerist";
  home.homeDirectory = "/home/avengerist";


  home.stateVersion = "24.11";
  programs.home-manager.enable = true;


  home.packages =
  (with pkgs; [
    neofetch
    keepassxc
    zsh
    kitty
    git
    wofi
    pavucontrol
    slack
    gnupg
    ripgrep
    telegram-desktop
    sway-contrib.grimshot
    nodejs
    tree-sitter
    obsidian
    zathura
    obs-studio
    remmina
    libreoffice
    brightnessctl
  ])

  ++

  (with pkgs-unstable; [
    vesktop
    satty
    d2
    flameshot
  ]);

  gtk = {
    enable = true;
    theme = {
     name = "adwaita-dark";
    };
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

}
