{ config, pkgs, pkgs-unstable, ...}:

{

  imports = [
    ../../modules/home/hyprland
    ../../modules/home/waybar
    ../../modules/home/kitty
    ../../modules/home/firefox.nix
    ../../modules/home/editor/neovim
    ../../modules/home/alacritty
  ];

  home.username = "avengerist";
  home.homeDirectory = "/home/avengerist";


  home.stateVersion = "24.11";
  programs.home-manager.enable = true;


  home.packages =
  (with pkgs; [
    neofetch
    keepassxc
    zsh
    kitty
    git
    wofi
    pavucontrol
    slack
    gnupg
    ripgrep
    sway-contrib.grimshot
    nodejs
    tree-sitter
    zathura
    remmina
    libreoffice
    brightnessctl
  ])

  ++

  (with pkgs-unstable; [
    flameshot
    ciscoPacketTracer8
    terraform
    kubectl
    k9s
    teams-for-linux
  ]);

  gtk = {
    enable = true;
    theme = {
     name = "adwaita-dark";
    };
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

}
