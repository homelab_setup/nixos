# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, pkgs-unstable, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ../../modules/system/security/ssh.nix
      ../../modules/system/editor/neovim
      ../../modules/system/fonts
      #../../modules/system/dm/sddm.nix
      ../../modules/system/dm/greetd.nix
      ../../modules/system/wm/hyprland.nix
      ../../modules/system/networking/wireguard
      ../../modules/system/shell/zsh
    ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Use the systemd-boot EFI boot loader.
  #boot.loader.systemd-boot.enable = true;
  boot.initrd.kernelModules = [ "i915" ];
  services.xserver.videoDrivers=["intel"];

  boot.loader.grub = {
    enable = true;
    device = "nodev";
    efiSupport = true;
    useOSProber = true; 
    default = 2;
    #extraConfig = ''
    #set superusers=""
    #'';
  };

  boot.loader.efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot/efi";
  };
 

  networking.hostName = "work-pc"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/Tallinn";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    earlySetup = true;
    font = "ter-124b";
    keyMap = "us";
    packages = with pkgs; [
      terminus_font
    ];
  };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  #hardware.pulseaudio.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    #wireplumber.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.avengerist = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    home = "/home/avengerist";
  };


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
  (with pkgs; [
    vim
    git
    wget
    htop
    home-manager
    tmux
    intel-media-driver
    wlr-randr
  ])

  ++

  (with pkgs-unstable; [
    go
    gopls
    lua-language-server
    sops
    cargo
    gcc
  ]);

  system.stateVersion = "24.05";

  security.rtkit.enable = true;
}
