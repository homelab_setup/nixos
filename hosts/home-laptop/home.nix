{ config, pkgs, ...}:

{

  home.username = "avengerist";
  home.homeDirectory = "/home/avengerist";

  home.stateVersion = "23.11";
  programs.home-manager.enable = true;


  home.packages = with pkgs; [
    neofetch
    keepassxc
    zsh
    git
    firefox
    go
  ];

}
