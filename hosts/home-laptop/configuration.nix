{ config, lib, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ../../system/security/ssh.nix
      ../../system/editor/neovim.nix
      ../../system/dm/sddm.nix
      ../../system/wm/hyprland.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.grub.device = "nodev";

  networking.networkmanager.enable = true;

  time.timeZone = "Europe/Tallinn";
  i18n.defaultLocale = "en_US.UTF-8";

  networking.hostName = "work-laptop";

  users.users.avengerist = {
     isNormalUser = true;
     home = "/home/avengerist";
  };

  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    zsh
  ];


  services.openssh.enable = true;
  system.stateVersion = "23.11";
}
