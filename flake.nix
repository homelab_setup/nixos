{
  inputs = {

    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-unstable, home-manager, sops-nix, ... }@inputs:

    let
      system = "x86_64-linux";
      lib = nixpkgs.lib;

      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
      };

      pkgs-unstable = import nixpkgs-unstable {
        inherit system;
        config = { allowUnfree = true; };
      };

    in {
      nixosConfigurations = {
        home-laptop = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
             ./hosts/home-laptop
             home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.avengerist = import ./hosts/home-laptop/home.nix;
            }

          ];
        };

        work-laptop = nixpkgs.lib.nixosSystem {
          inherit system;
          inherit pkgs;

          modules = [
             ./hosts/work-laptop
             home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.avengerist = import ./hosts/work-laptop/home.nix;
              home-manager.extraSpecialArgs = {
                inherit pkgs-unstable;
              };
            }
            sops-nix.nixosModules.sops {
              sops = {
                defaultSopsFile = ./secrets/secrets.yaml;
                validateSopsFiles = true;
                age.keyFile = "/root/.config/sops/age/keys.txt";
                defaultSopsFormat = "yaml";
                secrets = {
                  "vpn/wireguardConfig" = {};
                };
              };
            }
          ];

          specialArgs = {
            inherit pkgs-unstable;
          };

        };
        work-pc = nixpkgs.lib.nixosSystem {
          inherit system;
          inherit pkgs;

          modules = [
            ./hosts/work-pc
            home-manager.nixosModules.home-manager {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.avengerist = import ./hosts/work-pc/home.nix;
              home-manager.extraSpecialArgs = {
                inherit pkgs-unstable;
              };
            }
          ];

          specialArgs = {
            inherit pkgs-unstable;
          };

        };
      };
    };
}
