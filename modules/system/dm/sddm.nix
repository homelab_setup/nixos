{pkgs, config, ...}:
{
   environment.systemPackages = with pkgs; [
     sddm
   ];

  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
    enableHidpi = true;
  };
}
