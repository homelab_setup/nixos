# zsh.nix
{ pkgs, ... }:
{
  users.defaultUserShell = pkgs.zsh;
  system.userActivationScripts.zshrc = "touch .zshrc";
  environment.shells = with pkgs; [ zsh ];

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    shellAliases = {
      ll = "ls -l";
    };
    ohMyZsh = {
      enable = true;
      plugins = ["git"];
      theme = "lukerandall";
    };
  };
}
