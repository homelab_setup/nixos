# wireguard.nix
{ lib, config, pkgs, ... }:

let cfg = config.module.wireguard; in {
  options = {
    module.wireguard.enable = lib.mkEnableOption "Enables wireguard";
    module.wireguard.configFile = lib.mkOption {
      type = lib.types.nullOr lib.types.str;
      default = null;
    };
  };

  config = lib.mkIf (cfg.configFile != null && cfg.enable) {
    networking = {
      wg-quick.interfaces.wg0.configFile = cfg.configFile;
      wireguard.enable = true;
    };
  };

}
