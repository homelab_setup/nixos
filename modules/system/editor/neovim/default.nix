# neovim.nix
{ config, programs, pkgs, ...}:

{
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
    configure = {
      customRC = ''
        set number
        syntax on
        set list
        syntax on
        colorscheme habamax
        set relativenumber
        set ts=2 sts=2 sw=2
        set expandtab
        set cursorline
        set copyindent
      '';
    };
  };
}
