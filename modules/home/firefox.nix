{ config, pkgs-unstable, ... }:
{
  programs.firefox = {
    enable = true;
    package = pkgs-unstable.firefox;

    profiles.default = {
      id = 0;
      isDefault = true;
      settings = {
        # Protection settings
        "privacy.trackingprotection.enabled" = true;
        "privacy.resistFingerprinting" = false;
        "privacy.firstparty.isolate" = true;
        "browser.privatebrowsing.autostart" = true;

        "browser.theme.dark-private-windows" = true;
        "browser.fullscreen.autohide" = false;

        # Extensions settings
        "extensions.allowPrivateBrowsingByDefault" = true;
        "extensions.formautofill.creditCards.enabled" = false;
        "extensions.pocket.enabled" = false;

        # Disable welcome splash
        "trailhead.firstrun.didSeeAboutWelcome" = "nofirstrun-empty";
        "browser.aboutwelcome.enabled" = false;

        "dom.forms.autocomplete.formautofill" = false;
        "dom.payments.defaults.saveAddress" = false;
        "media.ffmpeg.vaapi.enabled" = true;
        "cookiebanners.ui.desktop.enabled" = true;
        "browser.translations.automaticallyPopup" = false;
      };
    };

    policies = {
      DisableTelemetry = true;
      DisableFirefoxStudies = true;
      EnableTrackingProtection = {
        Value = true;
        Locked = true;
        Cryptomining = true;
        Fingerprinting = true;
      };

      DNSOverHTTPS = {
        Enabled = true;
        ProviderURL = "https://mozilla.cloudflare-dns.com/dns-query";
        Locked = true;
      };

      ExtensionSettings = {
       "uBlock0@raymondhill.net" = {
         install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
         installation_mode = "force_installed";
       };
       "{d7742d87-e61d-4b78-b8a1-b469842139fa}" = {
         install_url = "https://addons.mozilla.org/firefox/downloads/latest/vimium-ff/latest.xpi";
         installation_mode = "force_installed";
       };
       "addon@darkreader.org" = {
         install_url = "https://addons.mozilla.org/firefox/downloads/latest/darkreader/latest.xpi";
         installation_mode = "force_installed";
       };
      };
    };

  };
}
