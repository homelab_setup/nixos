{config, pkgs, ... }: 
{
  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = ''
    ${builtins.readFile ./hypr/hyprland.conf}
    '';
  };

  programs.hyprlock = {
    enable = true;
    extraConfig = ''
    ${builtins.readFile ./hypr/hyprlock.conf}
    '';
  };
}
