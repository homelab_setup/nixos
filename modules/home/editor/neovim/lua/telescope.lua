local function map(mode, combo, mapping, opts)
  local options = {noremap = true}
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, combo, mapping, options)
end

map('n',';f',':Telescope find_files <CR>', {noremap=true})
map('n',';r',':Telescope live_grep <CR>', {noremap=true})
map('n','\\\\',':Telescope buffers <CR>', {noremap=true})
