{pkgs, config, ...}:

{
  programs.neovim =
  let
    toLua = str: "lua << EOF\n${str}\nEOF\n";
    toLuaFile = file: "lua << EOF\n${builtins.readFile file}\nEOF\n";
  in
  {
    viAlias = true;
    vimAlias = true;
    enable = true;

    extraLuaConfig = ''
      ${builtins.readFile ./lua/options.lua}
      ${builtins.readFile ./lua/lsp.lua}
      ${builtins.readFile ./lua/telescope.lua}
      ${builtins.readFile ./lua/treesitter.lua}
    '';

    plugins = with pkgs.vimPlugins; [
      {
        plugin = awesome-vim-colorschemes;
        config = "colorscheme PaperColor";
      }

      nvim-treesitter.withAllGrammars
      vim-nix
      vim-go

      nvim-lspconfig
      nvim-cmp
      lspkind-nvim

      #Telescope
      popup-nvim
      plenary-nvim
      telescope-nvim
    ];
  };
}
