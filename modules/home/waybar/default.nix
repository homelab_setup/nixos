{ pkgs-unstable, lib, ... }:

{
  programs.waybar = {
    enable = true;
    package = pkgs-unstable.waybar;
    style = ''
    ${builtins.readFile ./style.css}
    '';
    settings = [{
      layer = "top";
      position = "top";

      modules-left = [ "custom/launcher" "hyprland/workspaces" ];
      modules-center = [ "clock" ] ;
      modules-right = [ "network" "cpu" "memory" "disk" "battery" "hyprland/language" ];

      "custom/launcher" = {
        format = "";
        tooltip = "false";
      };
      "hyprland/workspaces" = {
      	format = "{name}";
      	on-scroll-up = "hyprctl dispatch workspace e+1";
      	on-scroll-down = "hyprctl dispatch workspace e-1";
      };
      "clock" = {
        interval = 1;
        format = " {:%a, %d %b, %I:%M %p}";
        format-alt = " {:%d/%m}";
        tooltip = "true";
        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
      };
      "memory" = {
      	interval = 5;
      	format = " {}%";
        tooltip = true;
      };
      "cpu" = {
      	interval = 5;
      	format = " {usage:2}%";
        tooltip = true;
      };
      "disk" = {
      	interval = 5;
      	format = " {percentage_used}%";
      	format-alt = " {used}/{total} GiB";
      	path = "/";
      };
      "battery" = {
        states = {
          warning = 30;
          critical = 15;
        };
        format = "{icon} {capacity}%";
        format-icons = ["" "" "" "" ""];
        max-length = 25;
        on-click = "";
        tooltip = false;
      };
      "hyprland/language" = {
        format = "  {short}";
        format-us_101_qwerty_comma_dead = "US";
        format-ee_101_qwerty_comma_dead = "EE";
        format-ru_101_qwerty_comma_dead = "RU";
        #keyboard-name = "at-translated-set-2-keyboard";
        on-click = "hyprctl switchxkblayout at-translated-set-2-keyboard next";
      };
      "network" = {
        interface = "wlp0s20f3";
        format = "{ifname}";
        format-wifi = "{essid} ({signalStrength}%) ";
        format-disconnected = "";
        tooltip-format = "{ifname}";
        tooltip-format-wifi = "{essid} ({signalStrength}%) ";
        tooltip-format-disconnected = "Disconnected";
        max-length = 50;
      };
    }];
  };
}

