# alacritty.nix
{pkgs-unstable, config, ...}:
let
  themeFile = builtins.fromTOML (builtins.readFile ./catppuccin-mocha.toml);
  customFile = builtins.fromTOML (builtins.readFile ./custom.toml);
in
{
  programs.alacritty = {
    enable = true;
    settings = themeFile // customFile;
    package = pkgs-unstable.alacritty;
  };
}
