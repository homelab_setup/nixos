{pkgs, config, ...}:
{
  programs.kitty = {
    enable = true;
    settings = {
      confirm_os_window_close = 0;
      background_blur = 1;
      background_opacity = "0.5";
      enable_audio_bell = false;
      copy_on_select = true;
      clipboard_control = "write-clipboard read-clipboard write-primary read-primary";
    };
  };
}
